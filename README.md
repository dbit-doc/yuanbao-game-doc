# 人人遊戲系統接口文檔

# 文件目的
  - 本文檔介紹平台接入人人系統流程、接口參數規範、加密驗簽規則，並於文末提供各語言版本串接範例。


# 傳輸規範

  - 使用TCP/IP作為傳輸層協議
  - 使用HTTP作為應用層協議
  - 傳遞參數格式皆為json(須使用Content-type規範header)
  
  
# 安全規範

  - 所有接口必須校驗sign簽名值正確無誤,再處理業務邏輯
  - 建議平台將人人系統通知主機加入信任名單,防範其餘外來主機偽裝調用接口
  - token由平台方自行生成,有效時間可由平台自行定義,失效即表示玩家斷線
  
# MD5簽名規範

  - 將所有接口請求參數(sign除外)以param-value形式串接,並以param的開頭採ASCII由小至大排序並在最後押上key=MD5key(人人系統發放)
  - MD5簽名採用utf-8編碼,將結果轉字串取32位小寫作為sign帶入請求參數
  - 簽名原串範例：param1=value1&param2=value2&param3=value3&key=123456

# 接口說明
## 人人系統接口清單

  - launch-<a href="#launch(POST)">取得遊戲連結(POST)</a>
  
## 平台方接口清單

  - get-login-info                      -<a href="#get-login-info(POST)">取得玩家資訊(POST)</a>
  - update-balance(bet)                 -<a href="#update-balance(bet)(POST)">更動玩家餘額-下注(POST)</a>
  - update-balance(payout)              -<a href="#update-balance(payout)(POST)">更動玩家餘額-派彩(POST)</a>
  - update-balance(applyDealer)         -<a href="#update-balance(applyDealer)(POST)">更動玩家餘額-申請上莊(POST)</a>
  - update-balance(betRollBack)         -<a href="#update-balance(betRollBack)(POST)">更動玩家餘額-退回下注(POST)</a>
  - update-balance(payoutRollBack)      -<a href="#update-balance(payoutRollBack)(POST)">更動玩家餘額-退回派彩(POST)</a>
  - update-balance(applyDealerRollBack) -<a href="#update-balance(applyDealerRollBack)(POST)">更動玩家餘額-退回上莊申請(POST)</a>
  
## 參數說明(<a href="#parametersTable">參數說明</a>) 

### launch-取得遊戲連結(POST
#### Reuqest

- Method: **POST**
- URL: ```/launch/{device}```
- Headers： Content-Type:application/json
- Body:
```
{
	"spId": "PL0001",
	"productId": "racing",
	"returnUrl": "http://www.google.com",
	"token": "20180502zaauxfrr",
	"requestTime": "20180503152756",
	"logoUrl": "http://d87b.com/image/PC/logo.jpg",
	"storeUrl": "http://d87b.com/image/PC/store.html",
	"sign": "c08d3e4cb641fbbdc5b06b448dd22395",
	"chatRoomId": "1" (僅對戰類須此傳參)
}
```

#### Response
- Body
```
{
	"retCode": "0",
	"data": {
		"gameUrl": "http://d87b.com/racing/k6yo0+UWN588ZNEjvDnFOgUBh6DN5amHLLqwkatz5VM="
	}
}
```


### get-login-info-取得玩家資訊(POST)
#### Reuqest

- Method: **POST**
- URL: ```平台提供自定義接口位置```
- Headers： Content-Type:application/json
- Body:
```
{
	"token": "20180502zaauxfrr",
	"sign": "02f8fc61372977a104f7cfb6356977dd"
}
```

#### Response
- Body
```
{ 
	"retCode": "0",
	"data": {
		"account": "GA0000123",
		"name": "小胖子",
		"balance": 50000,
		"headerUrl": "0.png"
	}
}
```

### update-balance(bet)-更動玩家餘額-下注(POST)
#### Reuqest

- Method: **POST**
- URL: ```平台提供自定義接口位置```
- Headers： Content-Type:application/json
- Body:
```
{
	"action": "bet", 
	"betId": "12355",
	"amt": 5000,
	"token": "20180502zaauxfrr", 
	"sign": "02f8fc61372977a104f7cfb6356977dd",
	"gameName": "abcde",
	"gameNumber": "ABC201805022222222",
	"betContent": "[{\"betAmt\":100,\"betDetailId\":51705974,\"content\":\"small\"}]",
	"isDealer": "0" (僅對戰類須此傳參)
}
```

#### Response
- Body
```
{ 
	"retCode": "0",
	"data": {
		"balance": 50000
		(變動後餘額)
	}
}
```

### update-balance(payout)-更動玩家餘額-派彩(POST)
#### Reuqest

- Method: **POST**
- URL: ```平台提供自定義接口位置```
- Headers： Content-Type:application/json
- Body:
```
{
	"action": "payout", 
	"payoutId": "253565",
	"betId": "12355",
	"amt": 5000,
	"betAmt":2500, (僅對戰類須此傳參)
	"token": "20180502zaauxfrr", 
	"sign": "02f8fc61372977a104f7cfb6356977dd"
}
```

#### Response
- Body
```
{ 
	"retCode": "0",
	"data": {
		"balance": 50000
		(變動後餘額)
	}
}
```

### update-balance(applyDealer)-更動玩家餘額-申請上莊(POST)
＊ 對戰類才有此接口
#### Reuqest

- Method: **POST**
- URL: ```平台提供自定義接口位置```
- Headers： Content-Type:application/json
- Body:
```
{
	"action": "applyDealer", 
	"applyDealerId": "13218630",
	"amt": 5000,
	"token": "20180502zaauxfrr", 
	"sign": "02f8fc61372977a104f7cfb6356977dd"
}
```

#### Response
- Body
```
{ 
	"retCode": "0",
	"data": {
		"balance": 50000
		(變動後餘額)
	}
}
```

### update-balance(betRollBack)-更動玩家餘額-退回下注(POST)
#### Reuqest

- Method: **POST**
- URL: ```平台提供自定義接口位置```
- Headers： Content-Type:application/json
- Body:
```
{
	"action": "betRollBack", 
	"applyDealerId": "13218630",
	"amt": 5000,
	"token": "20180502zaauxfrr", 
	"sign": "02f8fc61372977a104f7cfb6356977dd"
}
```

#### Response
- Body
```
{ 
	"retCode": "0",
	"data": {
		"balance": 50000
		(變動後餘額)
	}
}
```

### update-balance(payoutRollBack)-更動玩家餘額-退回派彩(POST)
#### Reuqest

- Method: **POST**
- URL: ```平台提供自定義接口位置```
- Headers： Content-Type:application/json
- Body:
```
{
	"action": "payoutRollBack", 
	"applyDealerId": "13218630",
	"amt": 5000,
	"token": "20180502zaauxfrr", 
	"sign": "02f8fc61372977a104f7cfb6356977dd"
}
```

#### Response
- Body
```
{ 
	"retCode": "0",
	"data": {
		"balance": 50000
		(變動後餘額)
	}
}
```

### update-balance(applyDealerRollBack)-更動玩家餘額-退回上莊申請(POST)
＊ 對戰類才有此接口
#### Reuqest

- Method: **POST**
- URL: ```平台提供自定義接口位置```
- Headers： Content-Type:application/json
- Body:
```
{
	"action": "applyDealerRollBack", 
	"applyDealerId": "13218630",
	"amt": 5000,
	"token": "20180502zaauxfrr", 
	"sign": "02f8fc61372977a104f7cfb6356977dd"
}
```

#### Response
- Body
```
{ 
	"retCode": "0",
	"data": {
		"balance": 50000
		(變動後餘額)
	}
}
```

## 參數說明
|參數名稱|參數格式|參數說明|是否必填|
|:--|:--|:--|:--|
|device|String(8)|裝置(有以下枚舉值:pc、mobile)|是|
|spId|String(32)|平台ID(由人人系統發配)|是|
|productId|String(16)|產品ID(以下產品對照表以及 launch/transfer-page )|是|
|returnUrl|String(256)|遊戲內按上頁欲返回之頁面(須提供完整URL)|是|
|token|String(64)|由平台生成,啟動遊戲時帶入,進入遊戲後用作呼叫接口|是|
|requestTime|String(14)|請求時間(yyyyMMddHHmmss)|是|
|sign|String(32)|簽名(詳見簽名機制說明)|是|
|gameUrl|String(256)|遊戲入口連結(此為動態接口具有時效性)|是|
|account|String(32)|玩家帳號|是|
|name|String(32)|玩家暱稱|是|
|balance|Integer(11)|玩家餘額(單位:cent)|是|
|retCode|String(256)|錯誤碼:0為無錯誤,以外皆為錯誤|是|
|retMsg|String(256)|錯誤訊息:如錯誤碼不為0則有此訊息|否|
|headerUrl|String(256)|玩家頭像地址(須提供完整URL)|是|
|action|String(16)|餘額操作(有以下枚舉值:bet(扣點)、payout(加點)、betRollback(加點)、payoutRollback(扣點)、applyDealer(扣點)、applyDealerRollBack(加點)，分別對應下注、返彩、下注返還、返彩返還、申請上莊保證金、申請上莊保證金退還)|是|
|betId|String(32)|如action為bet、betRollback,  此欄位則為betId,如action為payout、payoutRollback此欄位則為payoutId,如action為applyDealer、applyDealerRollBack此欄位則為applyDealerId|是|
|amt|Integer(11)|變動金額(單位:cent)|是|
|betAmt|Integer(11)|玩家下注金額(單位:cent)(payout時使用)|是|
|storeUrl|String(256)|上分Url|是|
|logoUrl|String(256)|遊戲畫面logo(可選填,缺省則為人人娛樂)|否|
|chatRoomId|Integer(1)|對戰類遊戲房間號(有下枚舉值:1、2、3、4、5)|是|
|betContent|String(256)|注單內容|是|

## 產品對照表
|產品ID(productId)|遊戲名稱|
|:--|:--|
|racing|彩票-北京賽車|
|rowing|彩票-幸運飛艇|
|timetime|彩票-重慶時時彩|
|self-racing|彩票-極速賽車|
|gd11x5|彩票-廣東11選五|
|js11x5|彩票-江蘇11選五|
|jx11x5|彩票-江西11選五|
|ffc11x5|彩票-分分彩11選五|
|bjk3|彩票-北京快三|
|gsk3|彩票-甘肅快三|
|gxk3|彩票-廣西快三|
|hebk3|彩票-河北快三|
|hubk3|彩票-湖北快三|
|jsk3|彩票-江蘇快三|
|ffck3|彩票-分分彩快三|
|dragon-tiger|電子-龍虎|
|dice-bao|電子-骰寶|
|cowcow|對戰-牛牛|
|this-bar|對戰-二八槓|
|red-box|對戰-歡樂紅包|
|three-cards|對戰-三公|
|roulette|電子-女王的新衣|
|oppostie-dice-bao|電子-反圍骰|
|crazy-roulette|電子-瘋狂輪盤|
|american-roulette|電子-美式輪盤|
|launch/transfer-page|對戰類大廳|

## 注單內容(betContent)說明

### 11選5
|下注內容|對應類型||下注內容|對應類型|
|:--|:--|:--|:--|:--|
|first_01, second_01...|定位膽_號碼||N_01|不定位膽|
|THS_01_02_03|前3直選||THG_02_05_11|前3組選|
|TWS_03_09|前2直選||TWG_10_11|前2組選|
|S1W1_01|選一中一||S2W2_01_02|選二中二|
|S3W3_01_02_03|選三中三||S4W4_01_02_03_04|選四中四|
|S5W5_01_02_03_04_05|選五中五||S6W5_01_02_03_04_05_06|選六中五|
|S7W5_01_02_03_04_05_06_07|選七中五||S8W5_01_02_03_04_05_06_07_08|選八中五|

###### 定位膽說明：
    first:第一位, second:第二位, third:第三位


### 快三
|下注內容|對應類型||下注內容|對應類型||下注內容|對應類型|
|:--|:--|:--|:--|:--|:--|:--|:--|
|big|大||small|小||odd|單|
|even|雙||sum_08|和值||TWS_5_5_2|二同號單選|
|TWC_1|二同號複選||TWN_4_6|二不同號||THS_2|三同號單選|
|TNS|三同號通選||THCN|三連號通選||THN_1_3_5|三不同號|

### 極速賽車/北京賽車/幸運飛艇
|下注內容|對應類型||下注內容|對應類型||下注內容|對應類型|
|:--|:--|:--|:--|:--|:--|:--|:--|
|first_01, second_01...|定位膽_號碼||(first-tenth)_big|大||(first-tenth)_small|小|
|(first-tenth)_odd)|單||(first-tenth)_even|雙||(first-fifth)_D|龍|
|(first-fifth)_T|虎||sum_(big/small/odd/even/03-19)|冠亞軍和||02_06|冠亞二星|

###### 定位膽說明：
    first:冠軍, second:亞軍, third:季軍, fourth:第四名, fifth:第五名, sixth:第六名, senventh:第七名, eighth:第八名, ninth:第九名, tenth:第十名

### 重慶時時彩
|下注內容|對應類型||下注內容|對應類型||下注內容|對應類型|
|:--|:--|:--|:--|:--|:--|:--|:--|
|mil_01, tho_01...|定位膽_號碼||(one-mil)_big|大||(one-mil)_small|小|
|(one-mil)_odd)|單||(one-mil)_even|雙||02_06|二星|

###### 定位膽說明：
    one:個位, ten:十位, hun:百位, tho:千位, mil:萬位

### 龍虎
|下注內容|對應類型||下注內容|對應類型||下注內容|對應類型|
|:--|:--|:--|:--|:--|:--|:--|:--|
|betDragon|龍||betTiger|虎||betTie|和|
|betDragonOdd|龍單||betDragonEven|龍雙||betTigerOdd|虎單|
|betTigerEven|虎雙||betDragonRed|龍红||betDragonBlack|龍黑|
|betTigerRed|虎红||betTigerBlack|虎黑||||

### 骰寶
|下注內容|對應類型||下注內容|對應類型||下注內容|對應類型|
|:--|:--|:--|:--|:--|:--|:--|:--|
|small|小||big|大||sum-4(4~17)|和值4~17|
|triple-1(1~6)|指定圍骰1~6||triple-a|任意圍骰||count-1(1~6)|三軍1~6|

### 女王的新衣
|下注內容|對應類型||下注內容|對應類型||下注內容|對應類型|
|:--|:--|:--|:--|:--|:--|:--|:--|
|B|比基尼||A|洋装||G|礼服|
|R|王族|||||||

### 反圍骰
|下注內容|對應類型||下注內容|對應類型||下注內容|對應類型|
|:--|:--|:--|:--|:--|:--|:--|:--|
|big|大||small|小||odd|單|
|even|雙||notTri|反圍骰||||

### 瘋狂輪盤
|下注內容|對應類型||下注內容|對應類型||下注內容|對應類型|
|:--|:--|:--|:--|:--|:--|:--|:--|
|red|紅||black|黑||odd|單|
|even|雙||0|零||not0|非零|
|small|小||medium|中||big|大|


# 待更新項目

 - 須補全局錯誤碼
 
 
# 更新日誌

 - 2018/05/02	創建文檔	by davidccc
 - 2018/05/03	增加參數說明	by davidccc
 - 2018/05/25   MD5規範     by Engine
 - 2018/06/15   啟動連結可傳入自定義logo	by davidccc
 - 2018/06/27   可傳入自定義充值頁面	by lf2lf211
 - 2018/08/06   增加chatRoomId參數(對戰類遊戲)	by davidccc
 - 2019/04/01   修改update-balance接口細節 by Chiu Shih-Chuan
 - 2019/04/02   新增下注內容(betContent)對照表 by Chiu Shih-Chuan
 - 2019/04/09   修改下注內容(betContent)對照表 by Chiu Shih-Chuan
 - 2019/04/10   修改下注內容(betContent)對照表-新增重慶時時彩 by Chiu Shih-Chuan
 - 2019/04/19   刪除get-balance接口 by Chiu Shih-Chuan
 
# 附錄：

